# Access Control

Access control restricts access to resources. 
Resource owners define attribute-based access control policies for their endpoints and define the attribute values a subject must attest in order to grant access to the resource.

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10 - 2.7
