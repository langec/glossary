# Gaia-X Framework

The Gaia-X Framework provides an overall view of the Gaia-X Association pillars and [deliverables](gaia-x_deliverables.md), highlighting the elements that are mandatory to be [Gaia-X Compliant](gaia-x_compliance.md).

## References
- [Gaia-X Framework](https://docs.gaia-x.eu/framework/)
