# Gaia-X Open Source Software Community

The open-source Gaia-X Community comprises the entire Gaia-X network. Users and providers collaborate on the basis of Data Space Events, Hackathons, Techdives, Onboarding Webinars, Community Newsflashes and Summits. 

## References
- https://gitlab.com/gaia-x/gaia-x-community

