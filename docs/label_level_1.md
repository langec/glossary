# Label Level 1

With reference to the three [Gaia-X Basic Labels](gaia-x_basic_labels.md): in Label Level 1 Data protection, transparency, security, portability, and flexibility are guaranteed in line with the rules defined in the [Gaia-X Policy Rules Document](gaia-x_policy_rules_and_labelling_document.md) and the basic set of technical requirements derived from the [Gaia-X Architecture Document](gaia-x_architecture_document.md). For cybersecurity, with the minimum requirement being to meet ENISA’s European Cybersecurity Scheme - Basic Level.

# References
- [Gaia-X Labelling Framework](https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf)
