# Gaia-X Operating Model

The Gaia-X Operating Model is part of the [Gaia-X Architecture Document](gaia-x_architecture_document.md). It introduces the Gaia-X Ecosystem, Trust Anchors, Gaia-X Compliance, the usage of Gaia-X Labels, Gaia-X Self-Descriptions life-cycle and the Gaia-X Registry.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 6
