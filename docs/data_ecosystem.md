# Data ecosystem

Data [ecosystems](ecosystem.md) enable [Participants](participant.md) to leverage data as a strategic resource in an inter-organizational network without restrictions of a fixed defined partner or central keystone companies. Data ecosystems not only enable significant data value chain improvements, but provide the technical means to enable Data [Sovereignty](sovereignty.md)

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 7.1.4
