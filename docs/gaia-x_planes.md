# Gaia-X planes

The three planes ([Trust plane](trust_plane.md), [Management plane](management_plane.md), [Usage plane](usage_plane.md) represent three levels of interoperability and match the planes as described in the [NIST Cloud Federation Reference Architecture](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.500-332.pdf).

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 3.5
- [NIST Cloud Federation Reference Architecture](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.500-332.pdf)
