# Resources

The term describes in general the goods and objects of the [Gaia-X Ecosystem](gaia-x_ecosystem.md). See also [Resource](resource.md)

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.3
