# Self-Description

Self-Descriptions are [W3C Verifiable Presentations](https://www.w3.org/TR/vc-data-model/#presentations) in the [JSON-LD format](https://www.w3.org/TR/json-ld11/). Self-Descriptions comprise signed or unsigned arrays of one or more [Verifiable Credentials](https://www.w3.org/TR/vc-data-model/#credentials).

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 5.4

