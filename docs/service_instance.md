# Service Instance

A Service Instance is the instantiation of a [Service Offering](service_offering.md) at runtime, strictly bound to a version of a [Self-Description](self_description.md). 

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.7


