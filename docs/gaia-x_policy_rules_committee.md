# Gaia-X Policy Rules Committee (PRC)

The Policy Rules Committee (PRC) aims to translate the guiding principles of the Gaia-X initiative, e.g., transparency, data protection, cyber security, portability, and openness, into High-Level Objectives to safeguard the added value of the [Gaia-X Ecosystem](gaia-x_ecosystem.md).
Furthermore, the PRC has the role to monitor, integrate and define the relationship with EU regulations and external standards.

## References
https://gaia-x.eu/who-we-are/association/
