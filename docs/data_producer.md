# Data Producer

A natural or legal [participant](participant.md) who furnishes data to a [Data Provider](data_provider.md)

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10, 2.2 
