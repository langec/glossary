# Management plane

It is one of the three [Gaia-X planes](gaia-x_planes.md) representing three levels of [interoperability](interoperability.md).
It represents an extension of the common digital governance provided by the [Federators](federator.md) of the relevant [ecosystems](ecosystem.md).
Specific ecosystem governance rules are out of scope for [Gaia-X](gaia-x.md).

# References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 3.5
- [NIST Cloud Federation Reference Architecture](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.500-332.pdf)



