# Verifiable Credential

A verifiable credential is a tamper-evident [credential](credential.md) that has authorship that can be cryptographically verified. Verifiable credentials can be used to build [verifiable presentations](verifiable_presentation.md), which can also be cryptographically verified.

## References
- [W3C, Verifiable Credentials Data Model v1.1](https://www.w3.org/TR/vc-data-model/#dfn-verifiable-credentials)
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 5.1, 5.4

