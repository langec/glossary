# Self-Description Graph

Set of all [Self-Descriptions](self_description.md) in the "active" lifecycle state and all their typed relations with each other.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 5.3.1




