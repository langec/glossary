# Holder

A role an entity might perform by possessing one or more [verifiable credentials](verifiable_credentials.md) and generating presentations from them. A holder is usually, but not always, a subject of the verifiable credentials they are holding. Holders store their credentials in credential repositories.

## References
- [W3C, Verifiable Credentials Data Model v1.1](https://www.w3.org/TR/vc-data-model/#dfn-holders)
