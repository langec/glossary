# Label Level 2

With reference to the three [Gaia-X Basic Labels](gaia-x_basic_labels.md): in Label Level 2 the basic requirements from Level 1 are extended. It reflects a higher level of security, transparency of applicable legal rules and potential dependencies. The option of a service location in Europe must be provided to the consumer. Regarding cybersecurity, the minimum requirement will be to meet ENISA European Cybersecurity Scheme -
Substantial Level.

# References
- [Gaia-X Labelling Framework](https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf)
