# Self-Description Schema

Collection of class'[data schema](https://www.w3.org/TR/vc-data-model/#data-schemas) describing Gaia-X [entities](entity.md).

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 5.2
