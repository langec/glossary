# Gaia-X Lighthouse projects

Projects aiming at creating a data exchange platform built on transparency, trust, and openness, targeting multiple industries, that are Gaia-X early adopters.

## References
- https://gaia-x.eu/wp-content/uploads/2022/10/Lighthouse-V6_2911.2022.pdf
