# Gaia-X Identity, Credential and Access Management (ICAM) specifications

The document is a [Gaia-X deliverable](gaia-x_deliverables.md) aimed at describing the components for “Authorization & Authentication” which shall
deliver core functionalities for authorization, access management and authentication as well as services around it to Gaia-X [Participants](participant.md) with the purpose to join the trustful environment of the [Ecosystem](gaia-x_ecosystem.md).

## References
- [Gaia-X Framework](https://docs.gaia-x.eu/framework/)
- [Gaia-X Identity, Credentials and Access Management speifications](https://docs.gaia-x.eu/technical-committee/identity-credential-access-management/latest/)

## alias
- Identity, Credential and Access Management specifications
- ICAM specifications
