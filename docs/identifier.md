# Identifier

Identifiers in Gaia-X are [URIs](https://it.wikipedia.org/wiki/Uniform_Resource_Identifier) and follow the specification of RFC 3986.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 5.1
