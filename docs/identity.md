# Identity

An Identity is composed of a unique [Identifier](identifier.md) and an attribute or set of attributes that uniquely describe an
entity within a given context.
Gaia-X uses existing Identities and does not maintain them directly. Uniqueness is ensured by a specific Identifier format relying on properties of existing protocols.

## References
- [Gaia-X Architecture Document 22.10](https://docs.gaia-x.eu/technical-committee/architecture-document/22.10/)
