# Federation Services

The Federation Services are necessary to enable a [federation](federation.md) of infrastructure and data and to provide interoperability across Federations.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 7

