# Gaia-X Data Exchange Services specifications

The document is a [Gaia-X deliverable](gaia-x_deliverables.md) that provides specifications for Data Exchange Services, including high level architecture and key requirements for data value, trust and compliance.

## References
- [Gaia-X Framework](https://docs.gaia-x.eu/framework/)
- [Gaia-X Data Exchange Services specifications](https://docs.gaia-x.eu/technical-committee/data-exchange/latest/)

## alias
- Data Exchange Services specifications
