# Gaia-X Policy Rules

The Policy Rules define high-level objectives safeguarding the added value and principles of the [Gaia-X Ecosystem](gaia-x_ecosystem.md).

## References
- [Gaia-X Policy Rules and Labelling Document](link) 22.11

## alias
- Policy Rules
