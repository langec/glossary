# Federation (principle)

Federation technically enables connections and a web of trust between and among different parties in the ecosystem(s).

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 7.1.5
