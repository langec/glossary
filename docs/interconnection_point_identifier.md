# Interconnection Point Identifier (IPI)

An Interconnection Point Identifier is a specific [Service Access Point](https://en.wikipedia.org/wiki/Service_Access_Point) that identifies where resources can interconnect.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.4
