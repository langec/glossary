# Trust plane

It is one of the three [Gaia-X planes](gaia-x_planes.md) representing three levels of [interoperability](interoperability.md).
It represents the global digital governance that is shared across ecosystems. The rules of this common governance are captured by the [Trust Framework](gaia-x_trust_framework.md) and operationalized by the [Gaia-X Compliance service](gaia-x_compliance_service) and the [Gaia-X Registry service](gaia-x_registry.md).

# References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 3.5
- [NIST Cloud Federation Reference Architecture](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.500-332.pdf)
