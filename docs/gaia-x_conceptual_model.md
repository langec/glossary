# Gaia-X Conceptual Model

The Gaia-X Conceptual Model is part of the [Gaia-X Architecture Document](gaia-x_architecture_document.md). It describes all concepts in the scope of [Gaia-X] and relations among them.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4
