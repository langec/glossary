# Consumer

A Consumer is a [Participant](participant.md) who searches [Service Offerings](service_offering.md) and consumes [Service Instances](service-instance.md) in the [Gaia-X Ecosystem](gaia-x_ecosystem.md) to enable digital offerings for [End-Users](end-user.md).

## References
- [Gaia-X Architecture Document 22.10](https://gaia-x.gitlab.io/technical-committee/architecture-document/) - 4.1.3
