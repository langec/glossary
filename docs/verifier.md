# Verifier

A role an entity performs by receiving one or more [verifiable credentials](verifiable_credential.md), optionally inside a [verifiable presentation](verifiable_presentation.md) for processing.

## References
- [W3C, Verifiable Credentials Data Model v1.1](https://www.w3.org/TR/vc-data-model/#dfn-verifier)
