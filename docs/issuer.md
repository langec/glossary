# Issuer

A role an entity can perform by asserting [claims](claim.md) about one or more subjects, creating a [verifiable credential](verifiable_credential.md) from these claims, and transmitting the verifiable credential to a [holder](holder.md).

## References
- [W3C, Verifiable Credentials Data Model v1.1](https://www.w3.org/TR/vc-data-model/#dfn-issuers)
