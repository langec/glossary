# Term of Usage (ref. Data Exchange) 

A specific instantiation of a [Data Licence](data_license.md) included in a Signed [Data Product Self-Description](data_product_self-description.md) listing all the constraints associated with a data exchange.

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10 - 2.2 
