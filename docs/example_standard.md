# Example Standard (ref. Policy Rules and Labelling Document)

In the context of the definition of Gaia-X Labelling Criteria (see [Policy Rules and Labelling Document](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-labelling/latest/policy_rules_labeling_document/), Example Standards shall identify potential means of implementation.
Example standards shall provide for possibilities how criteria may be implemented. Implementation as provided by such standards is not mandatory. Nor it is required to comply with any such standards. Gaia-X will provide additional notes, if significant differences will be identified. Example Standards shall especially help in evaluating conformity with Gaia-X, as Example Standards can be considered “implementation guidance”. 

## References
- [Gaia-X Policy Rules and Labelling Document](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-labelling/latest/policy_rules_labeling_document/), 2.2.5
