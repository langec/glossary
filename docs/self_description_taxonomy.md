# Self-Description Taxonomy

The [Self-Description Schema](self_description_schema.md) defines [entities](entity.md) that are recognized within Gaia-X. Those entities form an inheritance structure, whereas each entity inherits from one entity of the [Conceptual Model](conceptual_model.md). We call this inheritance hierarchy Self-Description Taxonomy.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 5.2

