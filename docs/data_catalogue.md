# Data Catalogue

A Data Catalogue presents a set of available [Data](data.md) and [Data Products](data_product.md) that can be queried.

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10, 2.2 
