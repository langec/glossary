# Data Portability

This refers to the porting of [data](data.md)(structured or unstructured) from one cloud service to another, public or private.

## References
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/) 22.10 - 6.7.2
- [ISO/IEC 19941:2017](https://www.iso.org/standard/66639.html) 5.2.2
