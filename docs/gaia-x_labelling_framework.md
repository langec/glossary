# Gaia-X Labelling Framework

The Gaia-X Labelling Framework is a Gaia-X document that explains the purpose and the value of Labels, the Labelling principles, the roles of [Label Owners](label_owner.md) and [Label Issuers](label_issuer.md), and the [Gaia-X Basic Labels](gaia-x_basic_label.md).
It derives from the requirements of the three Gaia-X Committees ([Technical Committee](technical_committee.md), [Policy Rules Committee](policy_rules_committee.md), [Data Spaces Business Committee](data_spaces_business_committee.md)).

## References
- [Gaia-X Labelling Framework](https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf)

