# Trusted Data Source

Source of the information used by the [issuer](trust_anchor.md) to validate attestations. The [Gaia-X Association](gaia-x.md) defines the list of Trusted Data Sources for Gaia-X Compliance.

## References
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/) 22.10 - 4
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 6.2, 6.3


