# Data Licensor 

A natural or legal participant (not necessarily a Gaia-X participant) who own usage rights for some data. It can be a data subject as per GDPR for personal data or a primary owner of non-personal data (i.e. not liable to GDPR).

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10, 2.2 
