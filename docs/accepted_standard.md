# Accepted Standard (ref. Policy Rules and Labelling Document)

In the context of the definition of Gaia-X Labelling Criteria (see [Policy Rules and Labelling Document](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-labelling/latest/policy_rules_labeling_document/), Accepted Standards shall be considered prima facie evidence of sufficient implementation of Gaia-X criteria.

## References
- [Gaia-X Policy Rules and Labelling Document](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-labelling/latest/policy_rules_labeling_document/), Proof of Concept/Bootstrapping
