# Labelling Criteria

All the criteria that define the different levels of [Gaia-X Basic Labels](gaia-x_basic_labels.md) are defined in detail in the [Policy Rules and Labelling Document](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-labelling/latest/policy_rules_labeling_document/).
The criteria are defined by attributes, which are split in different categories (data protection, transparency, security, portability and 
flexibility, European control).

## References
- [Gaia-X Labelling Framework](https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf)
