# Point Of Reference Standards (PORS)

In the context of the definition of the Gaia-X Labelling Criteria (see [Policy Rules and Labelling Document](link) the Point of Reference Standards 
shall provide a first impression on existing documents, i.e., standards, conformity assessment programmes, authorities' guidelines, procurement guidelines, etc. 

## References
- [Policy Rules and Labelling Document](link), Design Principles for Labels
