# Data License 

A contract template which contains the constraints (terms and conditions) associated with the data included in the Data Product. All the terms and conditions of the Data Usage Consent must be subsumed in the Data Licence for all data included in the product.

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10, 2.2 
