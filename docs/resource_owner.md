# Resource Owner

Natural or legal person, who holds the rights to [Resources](resource.md) that will be provided according to Gaia-X regulations by a [Provider](provider.md) and legally enable its provision. As Resources are bundled into a [Service Offering](service_offering) and nested Resource compositions can be possible, there is no separate resource owner either. 


## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.9.1
