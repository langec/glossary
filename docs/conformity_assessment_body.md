# Conformity Assessment Body (CAB)

Body that performs [Conformity Assessment](conformity_assessment.md) services. The Gaia-X Association reserves its right to choose its own CABs of its own three basic labels. 

## References
- DIN EN ISO/IEC 17000
- [Gaia-X Policy Rules and Labelling Document 22.11](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-labelling/22.11/) - Standards, self-assessment and Conformity Assessment Bodies (CAB)
