# Entity

An entity is an item relevant for the purpose of operation of a domain that has recognizably distinct existence.

## References
- [ISO/IEC 24760-1:2019](https://www.iso.org/standard/77582.html)
