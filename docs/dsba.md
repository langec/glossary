# DSBA (Data Spaces Business Alliance)

The Data Spaces Business Alliance (DSBA) is a collaboration initiative aimed at accelerating business transformation in the data economy.
The Alliance involves the [Gaia-X Association](gaia-x.md), the [Big Data Value Association](https://www.bdva.eu/) (BDVA), [FIWARE Foundation](https://www.fiware.org/), and the [International Data Spaces Association](https://internationaldataspaces.org/) (IDSA). 

## References
- https://data-spaces-business-alliance.eu/
