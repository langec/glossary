# Vertical Ecosystems

Vertical ecosystems are domain-specific [ecosystems](ecosystem.md) aimed at sharing data space knowledge and collecting/facilitating the development of cross-country use-cases.

## References
- https://gaia-x.eu/who-we-are/vertical-ecosystems/

