# Catalogue

A Catalogue service is a subclass of [Service Offering](service_offering.md) used to browse, search, filter [services](service_offering.md) and [resources](resource.md).

The goal of Catalogues is to enable [Consumers](consumer.md) to find best-matching offerings and to monitor for relevant
changes of the offerings. The [Providers](provider.md) decide in a self-sovereign manner which information they want to
make public in a Catalogue and which information they only want to share privately.

## References
- [Gaia-X Trust Framework](gaia-x_trust_framework.md) 22.10 - 6.6
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 7.4
