# Interoperability

This covers how the service offering can communicate with another service, such as making or responding to requests.

## References
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/) 22.10 - 6.7.1
- [ISO/IEC 19941:2017](https://www.iso.org/standard/66639.html)-5.2.1 
